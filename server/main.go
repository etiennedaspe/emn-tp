package main

import (
	"net/http"
	"sync"
)

const DEFAULT = "1"

var m = map[string]string{}
var lock = sync.RWMutex{}

func main() {
	router := http.NewServeMux()
	router.HandleFunc("/pull", pull)
	router.HandleFunc("/push", push)

	http.ListenAndServe("0.0.0.0:8080", router)
}

func pull(w http.ResponseWriter, r *http.Request) {
	group := r.URL.Query().Get("group")
	if group == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("group parameter should not be empty"))
		return
	}

	lock.RLock()
	defer lock.RUnlock()
	v, ok := m[group]
	if !ok {
		w.Write([]byte(DEFAULT))
	}
	w.Write([]byte(v))
}

func push(w http.ResponseWriter, r *http.Request) {
	group := r.URL.Query().Get("group")
	if group == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("group parameter should not be empty"))
		return
	}

	mode := r.URL.Query().Get("mode")
	if mode == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("mode parameter should not be empty"))
		return
	}

	lock.Lock()
	defer lock.Unlock()
	m[group] = mode
	w.Write([]byte(mode))
}
