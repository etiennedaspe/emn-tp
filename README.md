# Électronique

## Wemos D1
 * installer le driver WCH ;
    * Windows : http://www.wch-ic.com/downloads/CH341SER_EXE.html
    * MAC : http://www.wch-ic.com/downloads/CH341SER_MAC_ZIP.html
 * redémarrer l'ordinateur.

Ajouter les cartes esp8266 dans l'IDE arduino :
 * aller dans l'IDE Arduino, Fichier>Préférences>URL de gestionnaire de cartes supplémentaires ;
 * ajouter cette URL: https://arduino.esp8266.com/stable/package_esp8266com_index.json ;
 * redémarrer l'IDE Arduino.
 * Outils>Type de carte>Gestionnaire de cartes
 * Chercher esp8266 et installer.
 * Sélectionner le type de carte Wemos D1 R1
