int led = 13;
int button = 3;
int button_value = 0;
// Si state=true alors la led clignote, sinon elle est éteinte.
// C'est une variable booléenne (bool), elle peut prendre 2 valeurs : true et false.
bool state = false;
// Variables pouvant contenir une valeur temporelle.
long point_de_depart;
long maintenant;

void setup() {
  pinMode(led, OUTPUT);
  // Configuration du pin n°3 en mode INPUT,
  // c'est à dire que nous allons lire le signal sur le pin 3.
  pinMode(button, INPUT);
  // La fonction millis donne l'heure actuelle.
  maintenant = millis();
  point_de_depart = maintenant;
}

// Déclaration de la fonction blink.
void blink() {
  // S'il s'est écoulé moins de 500ms depuis le point de départ.
  if (maintenant - point_de_depart < 500) {
    // On allume la led.
    digitalWrite(led, HIGH);
  } else {
    // Sinon
    // S'il s'est écoulé moins de 1000ms
    if (maintenant - point_de_depart < 1000) {
      // On éteint la led.
      digitalWrite(led, LOW);
    } else {
      // Sinon on réinitialise notre point de départ avec l'heure actuelle.
      point_de_depart = maintenant;
    }
  }
}

void loop() {
  // Stockage de l'heure actuelle dans la variable maintenant.
  maintenant = millis();
  
  // Si state vaut "true"
  if (state) {
    // Appel de la fonction blink.
    blink();
  }
  
  // Lecture du signal sur le pin 3 et stockage de la valeur dans la variable button_value.
  button_value = digitalRead(button);
  // Si le courant pas (le bouton est enfoncé).
  if (button_value == HIGH) {
    // On change l'état (si state=true alors !state=false).
    state = !state;
    // On éteint la led.
    digitalWrite(led, LOW);
  }
}
