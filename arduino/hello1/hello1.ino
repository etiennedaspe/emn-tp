// La fonction setup est exécutée une seule fois au démarrage de l'Arduino.
void setup() {
  // Initialisation de la liaison série avec l'ordinateur (via la câble USB).
  Serial.begin(9600);
  // Envoi de la chaîne de caractères "Hello EMN !" à l'ordinateur via la liaison série.
  Serial.println("Hello EMN !");
}

void loop() {
}
