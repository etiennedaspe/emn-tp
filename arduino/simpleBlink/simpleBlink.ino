// Déclaration de la variable "led" de type "nombre entier" et affectation de la valeur 13.
int led = 13;

void setup() {
  // Configuration du pin n°13 (valeur de la variable led) en mode OUTPUT,
  // c'est à dire que nous allons écrire sur le pin 13.
  pinMode(led, OUTPUT);
}

void loop() {
  // Ecriture d'un signal "haut" sur le pin 13 (courant 5V).
  digitalWrite(led, HIGH);
  // Mise en pause de l'Arduino pendant 300ms.
  delay(300);
  // Ecriture d'un signal "bas" sur le pin 13 (pas de courant).
  digitalWrite(led, LOW);
  delay(300);
}
