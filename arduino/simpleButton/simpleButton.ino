int led = 13;
int button = 2;

int button_value = 0;

bool state = false;

void setup() {
  // put your setup code here, to run once:
  pinMode(led, OUTPUT);
  pinMode(button, INPUT);
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (state) {
    digitalWrite(led, HIGH);
  } else {
    digitalWrite(led, LOW);
  }
  
  button_value = digitalRead(button);
  if (button_value == HIGH) {
    state = !state;
  }
  delay(100);
}
