int led = 13;
int button = 3;
int button_value = 0;
// state peut prendre 4 valeurs : 0, 1, 2 et 3.
int state = 0;
long point_de_depart;
long maintenant;

void setup() {
  pinMode(led, OUTPUT);
  pinMode(button, INPUT);
  point_de_depart = millis();
  maintenant = point_de_depart;
}

// La fonction prend un paramètre interval qui est un nombre entier
void blink(int interval) {
    if (maintenant - point_de_depart < interval) {
      digitalWrite(led, HIGH);
    } else {
      if (maintenant - point_de_depart < interval * 2) {
        digitalWrite(led, LOW);
      } else {
        point_de_depart = maintenant;
      }
    }
}

void loop() {
  maintenant = millis();
  
  if (state > 0) {
    // Appel de la fonction blink avec le paramètre interval=state*500.
    blink(state * 500);
  }
  
  button_value = digitalRead(button);
  if (button_value == HIGH) {
    if (state == 3) {
      // Si state=3 alors on remet state à 0.
      state = 0;
    } else {
      // Sinon on incrémente state de 1.
      state = state + 1;
    }
    digitalWrite(led, LOW);
  }
  delay(100);
}
